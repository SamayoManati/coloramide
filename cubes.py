#! /usr/bin/env python3

# --------------------------------------------------------------------------------
#  Contrôles :
#    souris scroll haut : augmenter le nombre d'étages
#    souris scroll bas  : diminuer le nombre d'étages
#    flèche bas         : déplacer la vue vers le bas
#    flèche haut        : déplacer la vue vers le haut
#    B                  : active ou désactive le tracage des bordures
#    Z                  : zoom avant
#    Maj+Z              : zoom arrière
# --------------------------------------------------------------------------------

import sys
import json
import os.path
try:
	import pygame
except ModuleNotFoundError:
	print("Le module pygame est requis pour exécuter le programme")
	sys.exit(-1)

pygame.init()
pygame.display.set_caption("Cubes")

#SCREEN = pygame.display.set_mode((1080, 512))
SCREEN = pygame.display.set_mode((0,0), pygame.RESIZABLE)
SCREEN_WIDTH = SCREEN.get_width()
SCREEN_HEIGHT = SCREEN.get_height()
RUNNING = True

Y_OFFSET = 0
X_OFFSET = 0

if os.path.exists("cubes_config.json"):
	with open("cubes_config.json", "r") as fp:
		json_struct = json.loads(fp.read())
		N = json_struct["N"]
		R = json_struct["R"]
		r = json_struct["r"]
		DRAW_BORDERS = json_struct["borders"]["draw"]
		BORDERS_COLOR = tuple(json_struct["borders"]["color"])
		BORDERS_WIDTH = json_struct["borders"]["width"]
		RAINBOW_MODE = json_struct["rainbow_mode"]["actived"]
		RAINBOW_FACE_TOP_FROM = tuple(json_struct["rainbow_mode"]["faces"]["top"]["from"])
		RAINBOW_FACE_TOP_TO = tuple(json_struct["rainbow_mode"]["faces"]["top"]["to"])
		RAINBOW_FACE_LEFT_FROM = tuple(json_struct["rainbow_mode"]["faces"]["left"]["from"])
		RAINBOW_FACE_LEFT_TO = tuple(json_struct["rainbow_mode"]["faces"]["left"]["to"])
		RAINBOW_FACE_RIGHT_FROM = tuple(json_struct["rainbow_mode"]["faces"]["right"]["from"])
		RAINBOW_FACE_RIGHT_TO = tuple(json_struct["rainbow_mode"]["faces"]["right"]["to"])
# ---------
#  Options
#
#   Modifier les options ici écrasera les options du fichier de configuration.
#   Décommentez les lignes que vous voulez modifier.
# ---------
#N = 5  # Nombre d'étages
#R = 60 # Largeur de la grande arête du cube
#r = 25 # Largeur de la petite arête du cube

#DRAW_BORDERS  = True        # Si True, dessine les arêtes des cubes
#BORDERS_COLOR = Color.WHITE # La couleur des arêtes des cubes
#BORDERS_WIDTH = 1           # La largeur des arêtes des cubes

#RAINBOW_MODE            = True            # Colore les cubes en dégradé
#RAINBOW_FACE_TOP_FROM   = (255, 255, 255) # La couleur de départ pour la face supérieure
#RAINBOW_FACE_TOP_TO     = (0, -255, -255) # Le montant de l'addition de la couleur de départ
                                           #   pour obtenir la couleur de fin
#RAINBOW_FACE_LEFT_FROM  = (255, 255, 255) # La couleur de départ pour la face gauche
#RAINBOW_FACE_LEFT_TO    = (-255, 0, -255) # Le montant de l'addition de la couleur de départ
                                           #   pour obtenir la couleur de fin
#RAINBOW_FACE_RIGHT_FROM = (255, 255, 255) # La couleur de départ pour la face droite
#RAINBOW_FACE_RIGHT_TO   = (-255, -255, 0) # Le montant de l'addition de la couleur de départ
                                           #   pour obtenir la couleur de fin
# -----------------
#  Fin des options
# -----------------

pygame.display.set_caption("Cubes - " + str(N) + " étage(s)")

class Color:
	BLACK = (  0,   0,   0)
	WHITE = (255, 255, 255)
	RED   = (255,   0,   0)
	GREEN = (  0, 255,   0)
	BLUE  = (  0,   0, 255)
	GREY  = (128, 128, 128)

	def __init__(self):
		raise NotImplementedError("Cannot instanciate an enum")


def draw_cube(x: int, y: int, color: tuple, width: int = 1, fill: bool = False, fillColor = None, drawBorders: bool = True):
	"""x : la position X du sommet du cube

	y : la position Y du sommet du cube

	color : la couleur avec laquelle tracer les arêtes

	width : la largeur des arêtes

	fill : si True, remplit le cube

	fillColor : la couleur avec laquelle peindre l'intérieur du cube. Si None, prend la même couleur que pour les arêtes.
	fillColor peut être un tuple, auquel cas toutes les faces seront peintes de la même couleur, ou bien une liste de 3 tuples,
	auquel cas chaque face sera peinte différemment. L'index 0 correspond à la face du haut, l'index 1 à la face de gauche et
	l'index 1 à la face de droite.

	drawBorders : si False, ne trace pas les arêtes. True par défaut"""
	if fill:
		if fillColor is None:
			fillColor = [color, color, color]
		elif type(fillColor) is tuple:
			fillColor = [fillColor, fillColor, fillColor]
		pygame.draw.polygon(SCREEN, fillColor[0], [[x, y], [x+R, y], [x+R-r, y+r], [x-r, y+r]], 0)
		pygame.draw.polygon(SCREEN, fillColor[1], [[x-r, y+r], [x-r, y+r+R], [x+R-r, y+r+R], [x+R-r, y+r]], 0)
		pygame.draw.polygon(SCREEN, fillColor[2], [[x+R, y], [x+R-r, y+r], [x+R-r, y+r+R], [x+R, y+R]], 0)

	if drawBorders:
		pygame.draw.line(SCREEN, color, [x, y], [x+R, y], width)
		pygame.draw.line(SCREEN, color, [x+R-r, y+r], [x-r, y+r], width)
		pygame.draw.line(SCREEN, color, [x, y], [x-r, y+r], width)
		pygame.draw.line(SCREEN, color, [x+R, y], [x+R-r, y+r], width)
		pygame.draw.line(SCREEN, color, [x-r, y+r], [x-r, y+r+R], width)
		pygame.draw.line(SCREEN, color, [x+R-r, y+r], [x+R-r, y+r+R], width)
		pygame.draw.line(SCREEN, color, [x-r, y+r+R], [x+R-r, y+r+R], width)
		pygame.draw.line(SCREEN, color, [x+R, y], [x+R, y+R], width)
		pygame.draw.line(SCREEN, color, [x+R-r, y+R+r], [x+R, y+R], width)
	return

while RUNNING:
	SCREEN.fill(Color.BLACK)
	for ev in pygame.event.get():
		if ev.type == pygame.QUIT:
			RUNNING = False
		elif ev.type == pygame.MOUSEBUTTONDOWN:
			if ev.button == 4:
				# scroll vers le haut
				N += 1
				pygame.display.set_caption("Cubes - " + str(N) + " étage(s)")
			elif ev.button == 5:
				# scroll vers le bas
				if N > 1:
					N -= 1
					pygame.display.set_caption("Cubes - " + str(N) + " étage(s)")
		elif ev.type == pygame.KEYDOWN:
			if ev.key == pygame.K_DOWN:
				# flèche bas
				Y_OFFSET -= 2
			elif ev.key == pygame.K_UP:
				# flèche haut
				Y_OFFSET += 2
			elif ev.key == pygame.K_LEFT:
				# flèche gauche
				X_OFFSET += 2
			elif ev.key == pygame.K_RIGHT:
				# flèche droite
				X_OFFSET -= 2
			elif ev.key == pygame.K_b:
				DRAW_BORDERS = not DRAW_BORDERS
			elif ev.key == pygame.K_z:
				if ev.mod & pygame.KMOD_SHIFT:
					R *= 0.9
					r *= 0.9
				else:
					R *= 1.1
					r *= 1.1

	# Affichage des cubes
	current_floor = 1
	while current_floor <= N:
		x = SCREEN_WIDTH // 2
		for i in range(current_floor - 1): x -= r
		x += (R + r) * X_OFFSET
		y = (R + r) * (current_floor + Y_OFFSET)
		current_cube = 1
		while current_cube <= current_floor:
			if RAINBOW_MODE:
				face_top_r = RAINBOW_FACE_TOP_FROM[0] + ((RAINBOW_FACE_TOP_TO[0] / N) * current_floor)
				face_top_g = RAINBOW_FACE_TOP_FROM[1] + ((RAINBOW_FACE_TOP_TO[1] / N) * current_floor)
				face_top_b = RAINBOW_FACE_TOP_FROM[2] + ((RAINBOW_FACE_TOP_TO[2] / N) * current_floor)
				face_top = (face_top_r, face_top_g, face_top_b)

				face_left_r = RAINBOW_FACE_LEFT_FROM[0] + ((RAINBOW_FACE_LEFT_TO[0] / N) * current_floor)
				face_left_g = RAINBOW_FACE_LEFT_FROM[1] + ((RAINBOW_FACE_LEFT_TO[1] / N) * current_floor)
				face_left_b = RAINBOW_FACE_LEFT_FROM[2] + ((RAINBOW_FACE_LEFT_TO[2] / N) * current_floor)
				face_left = (face_left_r, face_left_g, face_left_b)

				face_right_r = RAINBOW_FACE_RIGHT_FROM[0] + ((RAINBOW_FACE_RIGHT_TO[0] / N) * current_floor)
				face_right_g = RAINBOW_FACE_RIGHT_FROM[1] + ((RAINBOW_FACE_RIGHT_TO[1] / N) * current_floor)
				face_right_b = RAINBOW_FACE_RIGHT_FROM[2] + ((RAINBOW_FACE_RIGHT_TO[2] / N) * current_floor)
				face_right = (face_right_r, face_right_g, face_right_b)

				draw_cube(x, y, BORDERS_COLOR, BORDERS_WIDTH, True, [face_top, face_left, face_right], DRAW_BORDERS)
			else:
				draw_cube(x, y, BORDERS_COLOR, BORDERS_WIDTH, False, drawBorders=DRAW_BORDERS)
			x += R + r
			y -= r
			current_cube += 1
		current_floor += 1
	pygame.display.flip()

sys.exit(0)